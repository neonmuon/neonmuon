using NeonMuon.Startup;

var builder = WebApplication.CreateBuilder(args);

builder.AddNeonMuon();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.MapNeonMuon();

app.Run();
