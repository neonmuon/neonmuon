using System.Reflection;
using System.Runtime.CompilerServices;

namespace NeonMuon.Utils;

public static class TypeHelpers
{
    public static bool IsAsyncMethod(this MethodInfo method)
    {
        var asyncStateMachineAttribute = method.GetCustomAttribute<AsyncStateMachineAttribute>();
        return asyncStateMachineAttribute != null;
    }
}