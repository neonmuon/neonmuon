namespace NeonMuon.Security;

public static class Permissions
{
    public const string PublicAccess = nameof(PublicAccess);
    public const string InternalAccess = nameof(InternalAccess);
    public const string AdminAccess = nameof(AdminAccess);
    public const string RootAccess = nameof(RootAccess);

    public const string RegisterUser = nameof(RegisterUser);
}
