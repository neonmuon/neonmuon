using NeonMuon.DataAccess;
using System.Security.Claims;

namespace NeonMuon.Security;

[Content("register.html")]
[Permission(Permissions.RegisterUser)]
public static class RegisterPage
{
    public static async Task<object> Post(
        string username,
        string password,
        Db db,
        CancellationToken cancellationToken
    )
    {
        var login = await db.Register(username, password, cancellationToken);

        if (login.Ticket == LoginTicket.Anonymous)
        {
            return BadRequest("Invalid username or password.");
        }

        var identity = new ClaimsIdentity(
            [
                new("sub", login.Id.ToString()),
                new("name", login.Username),
                new("loginticket",  login.Ticket.ToString()),
            ],
            "password",
            "name",
            "role"
        );

        var principal = new ClaimsPrincipal(identity);

        return SignIn(principal);
    }
}
