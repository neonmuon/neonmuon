using Microsoft.AspNetCore.Mvc;
using NeonMuon.DataAccess;
using System.Security.Claims;

namespace NeonMuon.Security;

[Content("login.html")]
[Permission(Permissions.PublicAccess)]
public static class LoginPage
{
    public static async Task<object> Post(
        [FromForm] string username,
        [FromForm] string password,
        Db db,
        CancellationToken cancellationToken
    )
    {
        var login = await db.Login(username, password, cancellationToken);

        if (login.Ticket == LoginTicket.Anonymous)
        {
            return BadRequest("Invalid username/email or password.");
        }

        var identity = new ClaimsIdentity(
            [
                new("sub", login.Id.ToString()),
                new("name", login.Username),
                new("loginticket",  login.Ticket.ToString()),
            ],
            "password",
            "name",
            "role"
        );

        var principal = new ClaimsPrincipal(identity);

        return SignIn(principal);
    }
}