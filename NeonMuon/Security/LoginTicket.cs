namespace NeonMuon.Security;

public record Login(Guid Id, string Username, LoginTicket Ticket)
{
    public static Login Anonymous { get; } = new(LoginTicket.Anonymous.User, "Anonymous", LoginTicket.Anonymous);
};

public readonly record struct LoginTicket
{
    private static readonly Guid AnonymousUser = Guid.Empty;

    public static LoginTicket Anonymous { get; } = new(DateTime.SpecifyKind(DateTime.MaxValue, DateTimeKind.Utc), AnonymousUser);

    public LoginTicket(DateTime expiration, Guid user)
    {
        Expiration = expiration;
        User = user;
        if (Expiration.Kind != DateTimeKind.Utc)
        {
            throw new InvalidOperationException("The expiration must be in UTC.");
        }
    }

    /// <summary>
    /// Format: [expiration]|[user]
    /// </summary>
    /// <param name="ticket"></param>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException"></exception>
    public static LoginTicket Parse(string ticket)
    {
        ArgumentNullException.ThrowIfNull(ticket);

        var segments = ticket.Split('|');

        if (segments.Length != 2)
        {
            throw new InvalidOperationException("Invalid format.");
        }

        if (!DateTimeOffset.TryParse(segments[0], out var expires))
        {
            throw new InvalidOperationException("Invalid expiration.");
        }

        if (!Guid.TryParse(segments[1], out var user))
        {
            throw new InvalidOperationException("Invalid user.");
        }

        return new(expires.UtcDateTime, user);
    }

    public DateTime Expiration { get; }
    public Guid User { get; }

    public bool HasExpired()
    {
        return Expiration < DateTime.UtcNow;
    }

    public override string ToString()
    {
        return $"{Expiration:s}Z|{User}";
    }
}