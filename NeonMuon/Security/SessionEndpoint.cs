using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace NeonMuon.Security;

[Content("session")]
[Permission(Permissions.PublicAccess)]
public static class SessionEndpoint
{
    public static object Get(
        HttpContext context
    )
    {
        return Json(new
        {
            sub = context.User.FindFirstValue("sub"),
            name = context.User.FindFirstValue("name"),
            roles = context.User.FindAll("role").Select(x => x.Value),
        });
    }
}
