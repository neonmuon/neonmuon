namespace NeonMuon.Security;

[Content("logout.html")]
[Permission(Permissions.PublicAccess)]
public static class LogoutPage
{
    public static object Get()
    {
        return SignOut();
    }
}