using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;
using System.Security.Cryptography;

namespace NeonMuon.Security;

public enum VerifyPasswordResult
{
    /// <summary>
    /// Indicates password verification failed.
    /// </summary>
    Failed = 0,

    /// <summary>
    /// Indicates password verification was successful.
    /// </summary>
    Success = 1,

    /// <summary>
    /// Indicates password verification was successful however the password was encoded using a deprecated algorithm
    /// and should be rehashed and updated.
    /// </summary>
    SuccessRehashNeeded = 2,
}

public static class PasswordHelper
{
    private const int iterationCount = 300_000;
    private static readonly object stubUser = new();
    private static readonly PasswordHasher<object> passwordHasher = new(Options.Create<PasswordHasherOptions>(new() { IterationCount = iterationCount }));

    /// <summary>
    /// Returns a hashed representation of the supplied <paramref name="password"/>. 
    /// </summary>
    /// <param name="plaintext"></param>
    /// <returns></returns>
    public static string Hash(string plaintext)
    {
        return passwordHasher.HashPassword(stubUser, plaintext);
    }

    /// <summary>
    /// Returns <see cref="VerifyPasswordResult.Success"/> or <see cref="VerifyPasswordResult.SuccessRehashNeeded"/> if the <paramref name="plaintext"/> is valid.
    /// </summary>
    /// <param name="hashtext"></param>
    /// <param name="plaintext"></param>
    /// <returns></returns>
    public static VerifyPasswordResult Verify(string hashtext, string plaintext)
    {
        return (VerifyPasswordResult)passwordHasher.VerifyHashedPassword(stubUser, hashtext, plaintext);
    }

    /// <summary>
    /// Returns a new password with the requested <paramref name="bitsOfEntropy"/>.
    /// </summary>
    /// <param name="bitsOfEntropy">Must be at least 64.</param>
    /// <returns></returns>
    /// <exception cref="ArgumentOutOfRangeException">Must request at least 64.</exception>
    public static string GeneratePassword(int bitsOfEntropy = 128)
    {
        if (bitsOfEntropy < 64)
            throw new ArgumentOutOfRangeException(nameof(bitsOfEntropy), bitsOfEntropy, $"The {nameof(bitsOfEntropy)} must be at least 64.");

        int numberOfBytes = (int)Math.Ceiling(bitsOfEntropy / 8f);
        byte[] bytes = RandomNumberGenerator.GetBytes(numberOfBytes);
        return WebEncoders.Base64UrlEncode(bytes);
    }
}
