namespace NeonMuon.Security;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
public class PermissionAttribute : Attribute
{
    public PermissionAttribute(string permission)
    {
        Permissions = [permission];
    }
    public PermissionAttribute(string permission0, params string[] permissions)
    {
        Permissions = [permission0, .. permissions];
    }

    public IReadOnlyCollection<string> Permissions { get; }
}