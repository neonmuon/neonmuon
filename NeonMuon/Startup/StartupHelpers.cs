using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NeonMuon.Content.Liquid;
using NeonMuon.DataAccess;
using NeonMuon.Security;
using Npgsql;
using System.Buffers;
using System.Security.Claims;
using System.Text.Json;

namespace NeonMuon.Startup;

public static class StartupHelpers
{
    public static void AddNeonMuon(this WebApplicationBuilder builder)
    {
        var neonMuonSection = builder.Configuration.GetRequiredSection(nameof(NeonMuon));
        var neonMuon = neonMuonSection.Get<NeonMuonOptions>()!;
        builder.Services.Configure<NeonMuonOptions>(neonMuonSection);

        builder.Services.AddHttpContextAccessor();
        builder.Services.AddSingleton<FileExtensionContentTypeProvider>();

        builder.Services.AddAuthentication().AddCookie(options =>
        {
            options.Cookie.Name = "auth_" + neonMuon.Id;
            options.LoginPath = "/login.html";
            options.LogoutPath = "/logout.html";
            options.AccessDeniedPath = "/login.html?forbidden=true";
        });
        builder.Services.AddAuthorization(options => options.FallbackPolicy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build());

        builder.Services.AddSingleton<LiquidRendering>();

        builder.Services.AddScoped<Login>(svc =>
        {
            var context = svc.GetRequiredService<IHttpContextAccessor>();
            var user = context.HttpContext?.User;

            if (user?.FindFirst("loginticket") is { } loginTicketClaim)
            {
                Guid id = Guid.Parse(user.FindFirstValue("sub")!);
                string username = user.FindFirstValue("name") ?? throw new NullReferenceException();
                return new Login(id, username, LoginTicket.Parse(loginTicketClaim.Value));
            }
            else
            {
                return Login.Anonymous;
            }
        });

        // Faster, more secure PostgreSQL: https://www.roji.org/parameters-batching-and-sql-rewriting
        AppContext.SetSwitch("Npgsql.EnableSqlRewriting", false);

        builder.Services.Configure<ConnectionStringsOptions>(builder.Configuration.GetRequiredSection("ConnectionStrings"));
        builder.Services.AddSingleton<InMemoryConnectionStringProvider>();
        builder.Services.AddSingleton<Db>();
    }

    public static void MapNeonMuon(this WebApplication app)
    {
        if (app.Environment.IsDevelopment())
        {
            NpgsqlLoggingConfiguration.InitializeLogging(app.Services.GetRequiredService<ILoggerFactory>());
        }

        // app.MapPost("login.html", login.Post).DisableAntiforgery().AllowAnonymous();

        // app.MapGet("logout.html", LogoutPage.Get).AllowAnonymous();

        app.MapGet("gen-password", () =>
        {
            var password = PasswordHelper.GeneratePassword();
            return new
            {
                password,
                hash = PasswordHelper.Hash(password),
            };
        }).AllowAnonymous();

        app.MapGet("query/{filename}.jsonl", QueryJsonl);
        app.MapGet("query/{filename}.json", QueryJson).AllowAnonymous();
        app.MapPut("query", BatchQuery);

        app.Map("{*path}", PageHandler.Delegate).AllowAnonymous();

        static async Task QueryJsonl(
            string sql,
            string filename,
            Login login,
            Db db,
            HttpResponse response,
            CancellationToken cancellationToken
        )
        {
            using var ds = await db.DataSource(login.Ticket, cancellationToken);
            var result = await ds.Query([new(sql, [])], cancellationToken);

            response.StatusCode = StatusCodes.Status200OK;
            response.ContentType = "application/jsonl";

            byte[] newline = [(byte)'\n'];
            foreach (var item in result[0].Rows)
            {
                var json = JsonSerializer.SerializeToUtf8Bytes(item);
                response.BodyWriter.Write(json);
                response.BodyWriter.Write(newline);
            }

            await response.BodyWriter.CompleteAsync();
        }


        static async Task<IResult> QueryJson(
            string sql,
            string filename,
            Login login,
            Db db,
            HttpResponse response,
            CancellationToken cancellationToken
        )
        {
            using var ds = await db.DataSource(login.Ticket, cancellationToken);
            var result = await ds.Query([new(sql, [])], cancellationToken);
            return Results.Json(result[0].Rows);
        }

        static async Task<List<SimpleCommandResult>> BatchQuery(
            IEnumerable<SimpleCommand> input,
            Login login,
            Db db,
            CancellationToken cancellationToken
        )
        {
            using var ds = await db.DataSource(login.Ticket, cancellationToken);
            var result = await ds.Query(input, cancellationToken);
            return result;
        }
    }
}
