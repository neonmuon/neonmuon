using System.Security.Claims;

namespace NeonMuon.Content;

public static class PageHelpers
{
    public static JsonResponse Json(object value)
    {
        return new(value);
    }

    public static BadRequestResponse BadRequest(string errorMessage)
    {
        return new(errorMessage);
    }

    public static SignInResponse SignIn(ClaimsPrincipal principal)
    {
        return new(principal);
    }

    public static SignOutResponse SignOut()
    {
        return new();
    }
}

public class BadRequestResponse(string errorMessage)
{
    public string ErrorMessage { get; } = errorMessage;
}

public class JsonResponse(object value)
{
    public object Value { get; } = value;
}

public class SignOutResponse { }

public class SignInResponse(ClaimsPrincipal principal)
{
    public ClaimsPrincipal Principal { get; } = principal;
}