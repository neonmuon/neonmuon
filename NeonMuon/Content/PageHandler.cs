using System.Reflection;
using System.Text;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using NeonMuon.Content.Liquid;
using NeonMuon.Utils;

namespace NeonMuon.Content;

public static class PageHandler
{
    private const string defaultPage = "index.html";
    private const string defaultExt = ".html";
    private static readonly string[] validFileExtensions = [".html", ".htm", ".js", ".css", ""];
    private static readonly string[] prefixes = ["", "_content/NeonMuon/"];

    private static readonly Dictionary<string, Type> ContentTypes = typeof(NeonMuonOptions).Assembly.GetExportedTypes()
        .Where(x => x.IsSealed && x.IsAbstract && x.IsClass)
        .Select(x => (Type: x, Content: x.GetCustomAttribute<ContentAttribute>()))
        .Where(x => x.Content != null)
        .ToDictionary(
            x => x.Content!.Path ?? (x.Type.Name[x.Type.Assembly.GetName().Name!.Length..].Replace('.', '/').ToLower() + defaultExt),
            x => x.Type
        );

    public static async Task<IResult> Delegate
    (
        string? path,
        IWebHostEnvironment environment,
        HttpContext context,
        FileExtensionContentTypeProvider contentTypeProvider,
        LiquidRendering liquidRendering,
        IServiceProvider serviceProvider,
        CancellationToken cancellationToken
    )
    {
        path = path?.ToLower();

        string ext;
        if (path == null || path.Length == 0)
        {
            path = defaultPage;
            ext = defaultExt;
        }
        else
        {
            ext = Path.GetExtension(path);
            if (!validFileExtensions.Contains(ext))
            {
                return Results.NotFound();
            }
        }

        IFileInfo fileInfo = default!;
        foreach (string prefix in prefixes)
        {
            fileInfo = environment.WebRootFileProvider.GetFileInfo(prefix + path);

            if (fileInfo.IsDirectory)
            {
                fileInfo = environment.WebRootFileProvider.GetFileInfo(prefix + path + "/" + defaultPage);
            }

            if (fileInfo.Exists)
            {
                break;
            }
        }

        bool actionFound = false;
        if (ContentTypes.TryGetValue(path, out var type))
        {
            string httpMethod = context.Request.Method.ToLower();

            var action = type.GetMethod(httpMethod, BindingFlags.Public | BindingFlags.Static | BindingFlags.IgnoreCase);

            if (action is not null)
            {
                actionFound = true;

                var parameters = action.GetParameters();

                var args = new object?[parameters.Length];

                int i = -1;
                foreach (var parameter in parameters)
                {
                    i++;

                    Type parameterType = parameter.ParameterType;

                    object? value;
                    if (parameterType == typeof(HttpContext))
                    {
                        value = context;
                    }
                    else if (parameterType == typeof(HttpRequest))
                    {
                        value = context.Request;
                    }
                    else if (parameterType == typeof(HttpResponse))
                    {
                        value = context.Response;
                    }
                    else if (parameterType == typeof(CancellationToken))
                    {
                        value = cancellationToken;
                    }
                    else
                    {
                        value = serviceProvider.GetService(parameterType);
                    }

                    if (value is null)
                    {
                        string name = parameter.Name ?? throw new NullReferenceException();

                        if (httpMethod == "get")
                        {
                            value = context.Request.Query.TryGetValue(name, out var val)
                                ? val
                                : parameter.DefaultValue;
                        }
                        else if (context.Request.HasFormContentType)
                        {
                            // TODO
                            var form = await context.Request.ReadFormAsync(cancellationToken);
                            value = Get(new(parameterType, parameter.DefaultValue, name), form);
                        }
                        else
                        {
                            value = await context.Request.ReadFromJsonAsync(parameterType, cancellationToken);
                        }
                    }

                    args[i] = value;
                }

                object? response;
                if (action.IsAsyncMethod())
                {
                    dynamic awaitable = action.Invoke(null, args)!;
                    response = await awaitable;
                }
                else
                {
                    response = action.Invoke(null, args);
                }

                switch (response)
                {
                    case BadRequestResponse badRequest:
                        if (context.Response.StatusCode < StatusCodes.Status400BadRequest)
                        {
                            context.Response.StatusCode = StatusCodes.Status400BadRequest;
                        }
                        context.Response.Headers.Append("x-error", badRequest.ErrorMessage);
                        break;

                    case JsonResponse json:
                        return Results.Json(json.Value);

                    case SignInResponse signIn:
                        await context.SignInAsync(signIn.Principal);
                        break;

                    case SignOutResponse:
                        await context.SignOutAsync();
                        break;

                    case null:
                        // Do nothing
                        break;

                    default:
                        throw new NotImplementedException("Action Response: " + response.GetType());
                }
            }
        }

        if (!fileInfo.Exists)
        {
            if (actionFound)
            {
                return Results.Empty;
            }

            return Results.NotFound();
        }

        if (!contentTypeProvider.TryGetContentType(fileInfo.Name, out var contentType))
        {
            contentType = "application/octet-stream";
        }

        var (error, content) = await liquidRendering.Render(fileInfo, new
        {
            query = context.Request.Query.ToDictionary(x => x.Key, x => x.Value),
        });

        if (error is not null)
        {
            // TODO: Log error
            if (environment.IsDevelopment())
            {
                return Results.Problem(error, statusCode: StatusCodes.Status500InternalServerError);
            }
            else
            {
                return Results.Problem("Unexpected error rendering page.", statusCode: StatusCodes.Status500InternalServerError);
            }
        }

        return Results.Content(content, contentType: contentType, contentEncoding: Encoding.UTF8);

        if (fileInfo.PhysicalPath is null)
        {
            return Results.Stream(fileInfo.CreateReadStream(), contentType: contentType, lastModified: fileInfo.LastModified, enableRangeProcessing: true);
        }

        return Results.File(fileInfo.PhysicalPath, contentType: contentType, lastModified: fileInfo.LastModified, enableRangeProcessing: true);

    }

    record GetFormValueOptions(Type Type, object? DefaultValue, string Prefix);

    private static object? Get(GetFormValueOptions options, IFormCollection form)
    {
        var (type, _, prefix) = options;

        if (!form.TryGetValue(prefix, out var stringValues))
        {
            return options.DefaultValue ?? Activator.CreateInstance(type);
        }

        if (type == typeof(string))
        {
            return stringValues.ToString();
        }
        else if (type.IsPrimitive)
        {
            return Convert.ChangeType(stringValues.ToString(), type);
        }

        throw new NotImplementedException(type.FullName);
    }
}