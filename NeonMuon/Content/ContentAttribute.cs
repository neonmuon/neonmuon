namespace NeonMuon.Content;

[AttributeUsage(AttributeTargets.Class)]
public class ContentAttribute(string? path = null) : Attribute
{
    public string? Path { get; } = path;
}