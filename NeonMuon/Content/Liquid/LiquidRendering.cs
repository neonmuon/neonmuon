using Fluid;
using Fluid.Ast;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using NeonMuon.DataAccess;
using NeonMuon.Security;
using Npgsql;
using System.Text;
using System.Text.Encodings.Web;

namespace NeonMuon.Content.Liquid;

public class LiquidRendering
{
    private readonly FluidParser parser;

    public LiquidRendering(IServiceProvider rootServiceProvider)
    {
        parser = new();
        parser.RegisterExpressionBlock("sql", async (value, statements, writer, encoder, context) =>
        {
            using var scope = rootServiceProvider.CreateScope();
            var db = scope.ServiceProvider.GetRequiredService<Db>();
            var login = scope.ServiceProvider.GetRequiredService<Login>();
            return await RenderSqlExpressionBlock(value, statements, context, db, login.Ticket);
        });
    }

    private static async ValueTask<Completion> RenderSqlExpressionBlock(
        Expression value,
        IReadOnlyList<Statement> statements,
        TemplateContext context,
        Db db,
        LoginTicket loginTicket)
    {
        // This feels hacky
        // string identifier = ((value as MemberExpression)?.Segments.FirstOrDefault() as IdentifierSegment)?.Identifier;
        StringWriter identifierWriter = new();
        var result = await value.EvaluateAsync(context);
        string identifier = result.ToStringValue();

        Dictionary<string, object?>[] values;
        {
            var (rewrittenStatements, parameterValues) = await RewriteSqlStatements(statements, context);

            StringWriter commandTextWriter = new();
            await rewrittenStatements.RenderStatementsAsync(commandTextWriter, null, context);
            string commandText = commandTextWriter.ToString();

            using NpgsqlDataSource dataSource = await db.DataSource(loginTicket, CancellationToken.None);

            using NpgsqlCommand cmd = dataSource.CreateCommand(commandText);
            foreach (var item in parameterValues)
            {
                cmd.Parameters.AddWithValue(item);
            }

            using NpgsqlDataReader reader = await cmd.ExecuteReaderAsync(CancellationToken.None);
            var cols = reader.GetColumnSchema();
            var columnNames = cols.Select(x => x.ColumnName).ToArray();
            var rows = await reader.RowsAsync(CancellationToken.None);

            // Fluid allows member access to Dictionary<string, object> by default
            // TODO: Create a more efficient data structure with equivalent behavior
            values = rows
                .Select(fields => columnNames.Zip(fields).ToDictionary(x => x.First, x => x.Second))
                .ToArray();
        }

        context.SetValue(identifier, values);

        return Completion.Normal;
    }

    private static async Task<(List<Statement> rewrittenStatements, List<string> parameterValues)> RewriteSqlStatements(IReadOnlyList<Statement> statements, TemplateContext context)
    {
        var rewrittenStatements = new List<Statement>();
        var parameters = new List<string>();

        foreach (var statement in statements)
        {
            if (statement is OutputStatement output)
            {
                var writer = new StringWriter();
                await new[] { output }.RenderStatementsAsync(writer, NullEncoder.Default, context);

                parameters.Add(writer.ToString());
                rewrittenStatements.Add(new TextSpanStatement("$" + parameters.Count));
            }
            else
            {
                rewrittenStatements.Add(statement);
            }
        }

        return (rewrittenStatements, parameters);
    }

    public async Task<(string? Error, string Result)> Render(IFileInfo file, object model)
    {
        using var stream = file.CreateReadStream();
        using var reader = new StreamReader(stream, Encoding.UTF8);
        var source = reader.ReadToEnd();

        if (parser.TryParse(source, out var template, out var error))
        {
            var context = new TemplateContext(model);
            var result = await template.RenderAsync(context, HtmlEncoder.Default);
            return (null, result);
        }
        else
        {
            return (error, string.Empty);
        }
    }
}