using Microsoft.Extensions.Options;
using NeonMuon.Security;
using Npgsql;

namespace NeonMuon.DataAccess;

public class Db(
    IOptions<NeonMuonOptions> NeonMuonOptions,
    IOptions<ConnectionStringsOptions> ConnectionStrings,
    InMemoryConnectionStringProvider DbLoginProvider
)
{
    private const int TicketLifetimeHours = 30 * 24;

    public async Task<NpgsqlDataSource> DataSource(LoginTicket ticket, CancellationToken cancellationToken)
    {
        var connectionString = await DbLoginProvider.GetConnectionString(ticket, cancellationToken);
        var dataSource = NpgsqlDataSource.Create(connectionString);
        return dataSource;
    }

    public async Task<Login> Login(string username, string password, CancellationToken cancellationToken)
    {
        username = username.Trim().ToLower();
        password = password.Trim();

        var connectionString = ConnectionStrings.Value["Admin"];
        using var ds = NpgsqlDataSource.Create(connectionString);

        var list = await ds.List<(Guid id, string name, string hash)>(
            $"""
            SELECT id, username, password
            FROM "user" JOIN user_password ON id = user_id
            WHERE username = {username}
            """
        , cancellationToken);

        if (list.Count != 1)
        {
            return Security.Login.Anonymous;
        }

        var (id, name, hash) = list[0];

        var verificationResult = PasswordHelper.Verify(hash, password);

        switch (verificationResult)
        {
            case VerifyPasswordResult.Success:
                break;

            case VerifyPasswordResult.Failed:
                return Security.Login.Anonymous;

            case VerifyPasswordResult.SuccessRehashNeeded:
                {
                    var rehashed = PasswordHelper.Hash(password);

                    await ds.Execute(
                        $"""
                        UPDATE SET password = {rehashed}
                        WHERE user_id = {id}
                        """
                    , cancellationToken);

                    break;
                }

            default:
                throw new NotSupportedException(nameof(VerifyPasswordResult) + ": " + verificationResult);
        }

        return new Login(id, name, new(DateTime.UtcNow.AddHours(TicketLifetimeHours), id));
    }

    public async Task<Login> Register(string username, string password, CancellationToken cancellationToken)
    {
        username = username.Trim().ToLower();
        password = password.Trim();

        if (username.Length < 3)
        {
            return Security.Login.Anonymous;
        }

        if (password.Length < 8)
        {
            return Security.Login.Anonymous;
        }

        Guid userId;

        {
            FormattableString createUserRecords = $"SELECT create_user({username}, {PasswordHelper.Hash(password)})";

            var adminConnectionString = ConnectionStrings.Value["Admin"];
            using var ds = NpgsqlDataSource.Create(adminConnectionString);
            userId = await ds.First<Guid>(createUserRecords, cancellationToken);
        }

        {
            string userRole = $"{NeonMuonOptions.Value.Id}:{userId}";
            string loginRole = $"{userRole}:login";

            var userIdentifier = Quote.Identifier(userRole);
            var loginIdentifier = Quote.Identifier(loginRole);
            var randomPasswordLiteral = Quote.Literal(SCRAMSHA256.EncryptPassword(PasswordHelper.GeneratePassword()));

            string createDatabaseUserAndLogin = $"""
CREATE ROLE {userIdentifier} WITH
    NOLOGIN
    NOSUPERUSER
    NOCREATEDB
    NOCREATEROLE
    INHERIT
    NOREPLICATION;

CREATE ROLE {loginIdentifier} WITH
    LOGIN
    NOSUPERUSER
    NOCREATEDB
    NOCREATEROLE
    INHERIT
    NOREPLICATION
    CONNECTION LIMIT -1
    IN ROLE {userIdentifier}
    ENCRYPTED PASSWORD {randomPasswordLiteral};

ALTER ROLE {loginIdentifier} SET ROLE TO {userIdentifier};
""";

            var securityRoot = ConnectionStrings.Value["SecurityRoot"];
            using var dataSource = NpgsqlDataSource.Create(securityRoot);
            await dataSource.Do([createDatabaseUserAndLogin], cancellationToken);
        }

        var login = await Login(username, password, cancellationToken);

        return login;
    }
}
