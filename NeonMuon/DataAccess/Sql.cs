using Npgsql;

namespace NeonMuon.DataAccess;

public class Sql
{
    public static implicit operator Sql(FormattableString sql)
    {
        return Format(sql);
    }

    public static implicit operator NpgsqlBatchCommand(Sql sql)
    {
        var (commandText, parameterValues) = sql.ToParameterizedSql();
        NpgsqlBatchCommand batchCommand = new(commandText);
        foreach (var value in parameterValues)
        {
            batchCommand.Parameters.Add(value);
        }

        return batchCommand;
    }

    private readonly string format;
    private readonly object?[] arguments;

    public Sql(string format, object?[] arguments)
    {
        this.format = format;
        this.arguments = arguments;
    }

    public string Unsanitized => format;

    public override string ToString()
    {
        var (commandText, _) = ToParameterizedSql();
        return commandText;
    }

    public (string commandText, NpgsqlParameter[] parameterValues) ToParameterizedSql()
    {
        var tempValues = new List<object>();
        var formatArgs = new List<string>(arguments.Length);

        foreach (var arg in arguments)
        {
            switch (arg)
            {
                case Sql sql:
                    formatArgs.Add(sql.GetParameterizedSql(ref tempValues));
                    break;

                default:
                    formatArgs.Add($"${tempValues.Count + 1}");
                    tempValues.Add(arg ?? DBNull.Value);
                    break;
            }
        }

        var parameterValues = tempValues
            .Select(val => val switch
            {
                char[] charArray => new NpgsqlParameter() { Value = charArray, NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.InternalChar },
                _ => new NpgsqlParameter() { Value = val },
            })
            .ToArray();
        var comandText = string.Format(format, args: formatArgs.ToArray());

        return (comandText, parameterValues);
    }

    private string GetParameterizedSql(ref List<object> parameterValues)
    {
        var formatArgs = new List<string>(arguments.Length);

        foreach (var arg in arguments)
        {
            switch (arg)
            {
                case Sql sql:
                    formatArgs.Add(sql.GetParameterizedSql(ref parameterValues));
                    break;

                default:
                    formatArgs.Add($"${parameterValues.Count + 1}");
                    parameterValues.Add(arg ?? DBNull.Value);
                    break;
            }
        }

        return string.Format(format, args: formatArgs.ToArray());
    }

    public static Sql Format(FormattableString formattableString)
    {
        return new(formattableString.Format, formattableString.GetArguments());
    }

    public static Sql Raw(string text)
    {
        return new(text, Array.Empty<object?>());
    }

    public static Sql Identifier(string text)
    {
        return Raw(Quote.Identifier(text));
    }

    /// <summary>
    /// Returns "<paramref name="prefix"/>"."<paramref name="text"/>".
    /// </summary>
    /// <param name="prefix"></param>
    /// <param name="text"></param>
    /// <returns></returns>
    public static Sql Identifier(string prefix, string text)
    {
        return Raw(Quote.Identifier(prefix, text));
    }

    /// <summary>
    /// Comma separated list of sanitized identifiers.
    /// </summary>
    /// <param name="texts"></param>
    /// <returns></returns>
    public static Sql IdentifierList(params string[] texts)
    {
        return IdentifierList((IEnumerable<string>)texts);
    }

    /// <summary>
    /// Comma separated list of sanitized identifiers.
    /// </summary>
    /// <param name="identifiers"></param>
    /// <returns></returns>
    public static Sql IdentifierList(IEnumerable<string> texts)
    {
        return Raw(string.Join(", ", texts.Select(Quote.Identifier)));
    }

    public static Sql Literal(string text)
    {
        return Raw(Quote.Literal(text));
    }

    public static Sql Literal(DateTime moment)
    {
        return Raw(Quote.Literal(moment));
    }
}