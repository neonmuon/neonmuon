using Microsoft.Extensions.Options;
using NeonMuon.Security;
using Npgsql;
using System.Collections.Concurrent;
using System.Security.Cryptography;

namespace NeonMuon.DataAccess;

public class InMemoryConnectionStringProvider(IOptions<NeonMuonOptions> NeonMuon, IOptions<ConnectionStringsOptions> ConnectionStrings)
{
    private readonly ConcurrentDictionary<LoginTicket, string> loginConnectionStrings = new();

    public async Task<string> GetConnectionString(LoginTicket ticket, CancellationToken cancellationToken)
    {
        if (ticket.HasExpired())
        {
            throw new ArgumentException("Ticket expired.", nameof(ticket));
        }

        if (loginConnectionStrings.TryGetValue(ticket, out var connectionString))
        {
            return connectionString;
        }

        string user = $"{NeonMuon.Value.Id}:{ticket.User}";
        string login = $"{user}:login";
        string password = Convert.ToBase64String(RandomNumberGenerator.GetBytes(16));

        {
            var loginIdentifier = Quote.Identifier(login);
            var loginLiteral = Quote.Literal(login);
            var newPasswordLiteral = Quote.Literal(SCRAMSHA256.EncryptPassword(password));
            var grantedRoleIdentifier = Quote.Identifier(user);

            var securityRoot = ConnectionStrings.Value["SecurityRoot"];
            using var dataSource = NpgsqlDataSource.Create(securityRoot);

            string script = $"""
IF EXISTS (
    SELECT FROM pg_catalog.pg_roles WHERE rolname = {loginLiteral}
) THEN
    ALTER ROLE {loginIdentifier} WITH ENCRYPTED PASSWORD {newPasswordLiteral};
ELSE
    BEGIN

        CREATE ROLE {loginIdentifier} WITH
            LOGIN
            NOSUPERUSER
            NOCREATEDB
            NOCREATEROLE
            INHERIT
            NOREPLICATION
            CONNECTION LIMIT -1
            IN ROLE {grantedRoleIdentifier}
            ENCRYPTED PASSWORD {newPasswordLiteral};

        ALTER ROLE {loginIdentifier} SET role TO {grantedRoleIdentifier};

    EXCEPTION
        WHEN duplicate_object THEN
            RAISE NOTICE 'Login was just created by a concurrent transaction. Skipping.';

    END;
END IF;
""";

            await dataSource.Do([script], cancellationToken);
        }

        var builder = new NpgsqlConnectionStringBuilder(ConnectionStrings.Value["Anonymous"])
        {
            Username = login,
            Password = password
        };
        connectionString = builder.ConnectionString;

        // Cache this connection string
        loginConnectionStrings[ticket] = connectionString;

        return connectionString;
    }
}
