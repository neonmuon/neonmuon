using Npgsql;
using System.Reflection;
using System.Text.Json;

namespace NeonMuon.DataAccess;


public class SimpleCommand
{
    public SimpleCommand(string sql, JsonElement[] parameters)
    {
        Sql = sql;
        Parameters = parameters;
    }

    public string Sql { get; init; }
    public JsonElement[] Parameters { get; init; }
}

public class SimpleCommandResult
{
    // public required IReadOnlyCollection<QueryColumn> Columns { get; set; }
    public required IReadOnlyCollection<object?[]> Rows { get; set; }
}

public static class NpgsqlExtensions
{
    public static async Task<int> Do(this NpgsqlDataSource dataSource, IEnumerable<string> scripts, CancellationToken cancellationToken)
    {
        var batch = dataSource.CreateBatch();
        foreach (var command in scripts)
        {
            var doTag = Random.Shared.Next();
            batch.BatchCommands.Add(new($"""
DO $$
BEGIN

{command}

END$$;
"""));
        }

        return await batch.ExecuteNonQueryAsync(cancellationToken);
    }

    public static async Task<int> Execute(this NpgsqlDataSource dataSource, FormattableString sql, CancellationToken cancellationToken)
    {
        using var batch = dataSource.CreateBatch();
        var sql1 = Sql.Format(sql);
        batch.BatchCommands.Add(sql1);

        var changes = await batch.ExecuteNonQueryAsync(cancellationToken);

        return changes;
    }

    public static async Task<List<SimpleCommandResult>> Query(this NpgsqlDataSource dataSource, IEnumerable<SimpleCommand> commands, CancellationToken cancellationToken)
    {
        using var batch = dataSource.CreateBatch();
        foreach (var command in commands)
        {
            NpgsqlBatchCommand cmd = new(command.Sql);
            foreach (var parameter in command.Parameters)
            {
                var clrValue = parameter.ValueKind switch
                {
                    _ => parameter.GetString(),
                };
                cmd.Parameters.Add(clrValue != null ? clrValue : DBNull.Value);
            }
            batch.BatchCommands.Add(cmd);
        }

        var result = new List<SimpleCommandResult>();
        await using (var reader = await batch.ExecuteReaderAsync(cancellationToken))
        {
            do
            {
                var rows = await reader.RowsAsync(cancellationToken);
                result.Add(new()
                {
                    Rows = rows,
                });
            }
            while (await reader.NextResultAsync(cancellationToken));
        }

        return result;
    }

    public static async Task<T> First<T>(this NpgsqlDataSource dataSource, FormattableString query, CancellationToken cancellationToken)
    {
        // TODO: Make better
        var list = await List<T>(dataSource, query, cancellationToken);
        return list.First();
    }

    public static async Task<List<T>> List<T>(this NpgsqlDataSource dataSource, FormattableString query, CancellationToken cancellationToken)
    {
        using var batch = dataSource.CreateBatch();
        batch.BatchCommands.Add(Sql.Format(query));

        await using var reader = await batch.ExecuteReaderAsync(cancellationToken);
        var list = await reader.List<T>(cancellationToken);
        return list;
    }


    public static async Task<List<T>> List<T>(
        this NpgsqlDataReader reader,
        CancellationToken cancellationToken
    )
    {
        Type type = typeof(T);

        bool isPrimitive = false;
        MethodInfo? ValueTuple_Create = null;
        if (type.Name.StartsWith("ValueTuple`"))
        {
            var args = type.GetFields().Select(f => f.FieldType).ToArray();
            var createT = typeof(ValueTuple)
                .GetMethods()
                .Single(m => m.Name == nameof(ValueTuple.Create) && m.GetParameters().Length == args.Length);
            ValueTuple_Create = createT.MakeGenericMethod(args);
        }
        else if (type.IsPrimitive || type == typeof(string))
        {
            isPrimitive = true;
        }

        var list = new List<T>();

        while (await reader.ReadAsync(cancellationToken))
        {
            object?[] record = new object?[reader.FieldCount];
            reader.GetValues(record!);

            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (record[i] == DBNull.Value)
                {
                    record[i] = null;
                }
                else if (record[i] is string text && reader.GetDataTypeName(i) == "json")
                {
                    record[i] = JsonDocument.Parse(text);
                }
            }

            if (ValueTuple_Create is not null)
            {
                T item = (T)ValueTuple_Create.Invoke(null, record)!;
                list.Add(item);
            }
            else if (isPrimitive)
            {
                T item = (T)record[0]!;
                list.Add(item);
            }
            else
            {
                T item = (T)Activator.CreateInstance(type, record)!;
                list.Add(item);
            }
        }

        return list;
    }

    public static async Task<List<object?[]>> RowsAsync(this NpgsqlDataReader reader, CancellationToken cancellationToken)
    {
        var list = new List<object?[]>();
        while (await reader.ReadAsync(cancellationToken))
        {
            object?[] record = new object?[reader.FieldCount];
            reader.GetValues(record!);
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (record[i] == DBNull.Value)
                {
                    record[i] = null;
                }
                else if (record[i] is string text && reader.GetDataTypeName(i) == "json")
                {
                    record[i] = JsonDocument.Parse(text);
                }
            }
            list.Add(record);
        }

        return list;
    }
}
